package main

import (
	"app/database"
	"app/util"
	"encoding/json"
	"fmt"
	"log"
	"net/http"

	"github.com/gorilla/mux"
	solr "github.com/rtt/Go-Solr"
)

var (
	sessionDB *solr.Connection
)

func main() {
	//จัดการ route ด้วย gorilla mux
	r := mux.NewRouter()
	r.HandleFunc("/", index).Methods("GET")
	//.. .
	//r.HandleFunc("/data/{core}", createCore).Methods("POST")              //can in cli but not in api
	r.HandleFunc("/data/{core}/query", queryCore).Methods("GET")              //fulltext, fuzzy ok //wildcard editing
	r.HandleFunc("/data/{core}/docs/query", createData).Methods("POST")       //ok for now thank you p'art
	r.HandleFunc("/data/{core}/docs/query", deleteData).Methods("DELETE")     //ok params id and its deleted
	r.HandleFunc("/data/{core}/query/{query_type}", queryType).Methods("GET") //put fuzzy wildcard or something to fulltext

	//รับ api ขึ้นมาตาม port docker
	log.Println("ListenAndServe api started :8080")
	http.ListenAndServe(":8080", r)
}

func index(w http.ResponseWriter, r *http.Request) {
	util.ResponseOk(w, 200, "[√] Service approximate search started.")
}

//Data ja
// type Data struct {
// 	Core   string `json:"core"`
// 	Result struct {
// 		Name  string  `json:"name"`
// 		Score float64 `json:"score"`
// 	}
// }

// func querySelect(w http.ResponseWriter, r *http.Request) {
// 	//รับค่าจาก route
// 	vars := mux.Vars(r)
// 	core := vars["core"]

// 	//กำหนดค่าให้ response เป็นค่ากำหนดเอง
// 	data := &Data{}
// 	data.Core = core
// 	data.Result.Name = "ทดสอบ"
// 	data.Result.Score = 6.88

// 	//response ข้อมูลออกเป็น json
// 	util.ResponseJSON(w, http.StatusOK, map[string]interface{}{
// 		"data":    data,
// 		"message": "data retrievedsuccess",
// 		"status":  "OK",
// 	})
// }

func queryCore(w http.ResponseWriter, r *http.Request) {

	vars := mux.Vars(r)
	core := vars["core"]
	keyword := r.URL.Query().Get("keyword") //params searching

	connect, err := database.ConnectSolr(core) //connect to solr core
	if err != nil {
		util.ResponseError(w, http.StatusBadRequest, "cant connect solr database.")
		return
	}
	sessionDB = connect //connect to solr db

	qry := solr.Query{ //alway query with fulltext, fuzzy
		Params: solr.URLParamMap{
			"q": []string{(keyword) + "~"},
		},
		Rows: 10, //setting row
	}

	res, err := sessionDB.Select(&qry) //select query result to response
	if err != nil {
		util.ResponseError(w, http.StatusBadRequest, "field data not found.")
		return
	}
	results := res.Results //set response results to result

	util.ResponseJSON(w, http.StatusOK, map[string]interface{}{ //show result
		"Search": keyword,
		"QTime":  res.QTime,
		"data":   results,
	})

}

func queryType(w http.ResponseWriter, r *http.Request) {

	vars := mux.Vars(r)
	core := vars["core"]
	queryType := vars["query_type"]
	keyword := r.URL.Query().Get("keyword")

	// log.Println(core)
	// log.Println(queryType)
	// log.Println(keyword)

	connect, err := database.ConnectSolr(core)
	if err != nil {
		util.ResponseError(w, http.StatusBadRequest, "cant connect solr database.")
		return
	}
	sessionDB = connect

	tp := queryType

	switch tp {
	case "fuzzy":
		qry := solr.Query{
			Params: solr.URLParamMap{
				"q": []string{(keyword) + "~"},
			},
			Rows: 10,
		}

		res, err := sessionDB.Select(&qry)
		if err != nil {
			util.ResponseError(w, http.StatusBadRequest, "fuzzy data not found.")
			return
		}

		util.ResponseJSON(w, http.StatusOK, map[string]interface{}{
			"Search": keyword,
			"QTime":  res.QTime,
			"data":   res.Results,
		})

	case "wildcard":
		qry := solr.Query{
			Params: solr.URLParamMap{
				"q": []string{(keyword) + "*"},
			},
			Rows: 10,
		}

		res, err := sessionDB.Select(&qry)
		if err != nil {
			util.ResponseError(w, http.StatusBadRequest, "wildcard data not found.")
			return
		}

		util.ResponseJSON(w, http.StatusOK, map[string]interface{}{
			"Search": keyword,
			"QTime":  res.QTime,
			"data":   res.Results,
		})

	default:
		qry := solr.Query{
			Params: solr.URLParamMap{
				"q": []string{(keyword)},
			},
			Rows: 10,
		}

		res, err := sessionDB.Select(&qry)
		if err != nil {
			util.ResponseError(w, http.StatusBadRequest, "field data not found.")
			return
		}

		util.ResponseJSON(w, http.StatusOK, map[string]interface{}{
			"Search": keyword,
			"QTime":  res.QTime,
			"data":   res.Results,
		})
	}
}

func createData(w http.ResponseWriter, r *http.Request) { //create data in existed core
	vars := mux.Vars(r)
	core := vars["core"]
	docs := r.FormValue("docs")
	connect, err := database.ConnectSolr(core) //connect to solr core
	if err != nil {
		util.ResponseError(w, http.StatusBadRequest, "cant connect solr database.")
		return
	}
	sessionDB = connect //connect to solr db

	var data []interface{} //set data to string form
	err = json.Unmarshal([]byte(docs), &data)
	if err != nil {
		util.ResponseError(w, http.StatusBadRequest, "docs is invalid json format.")
		return
	}

	create := map[string]interface{}{ //query to create docs in core
		"add": data,
	}

	_, err = sessionDB.Update(create, true) //set response
	if err != nil {
		message := fmt.Sprintf(`execute solr databasee fail: %s`, err)
		util.ResponseError(w, http.StatusBadRequest, message)
		return
	}

	util.ResponseJSON(w, http.StatusCreated, map[string]interface{}{ //show response
		"status":  "OK",
		"message": "created data in core success.",
	})

}

func deleteData(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	core := vars["core"]
	docID := r.URL.Query().Get("doc_id")

	connect, err := database.ConnectSolr(core)
	if err != nil {
		util.ResponseError(w, http.StatusBadRequest, "cant connect solr database.")
		return
	}
	sessionDB = connect

	id := map[string]interface{}{
		"delete": map[string]interface{}{
			"id": (docID),
		},
	}

	_, err = sessionDB.Update(id, true)
	if err != nil {
		message := fmt.Sprintf(`execute solr databasee fail: %s`, err)
		util.ResponseError(w, http.StatusBadRequest, message)
		return
	}

	util.ResponseJSON(w, http.StatusCreated, map[string]interface{}{
		"status":  "OK",
		"message": "delete data in core success.",
	})
}
