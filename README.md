# Docker-Approximate-Search

## Install prerequisites
ก่อนที่จะติดตั้งระบบให้ตรวจสอบให้แน่ใจว่าได้ทำการติดตั้งซอฟต์แวร์ ดังนี้
* [docker](https://docs.docker.com/engine/install/)
* [golang](https://golang.org/dl/)

## Installation
**Clone Repository**
```sh 
git clone https://gitlab.com/woranai-gitlab-101/approximate-search-api.git
```
**Change Directory**
```sh 
cd approximate-search
```

## Usage
**Run Container**
```sh 
go mod init app
```
**Run Container**
```sh 
docker-compose up -d
```
**Console Container**
```sh
docker exec -it <CONTAINER_ID> bash
```

## Endpoints 

**GET** http://localhost:8980/
-  API 

**GET** http://localhost:8983/
- Solr Database
